-- Tabel tabel di bawah ini tabel 'utama'; tidak punya
-- keterkaitan satu dengan yang lainnya.

-- Tabel surat digunakan untuk setiap surat-surat yang 
-- ada di dalam sistem. Silahkan lihat tabel-tabel
-- Tipe dan IO untuk informasi terkait surat yang lainnya.
CREATE TABLE Surat (
    -- Information Related
    id_surat INT NOT NULL AUTO_INCREMENT,
    no_surat VARCHAR(50) NOT NULL,
    perihal VARCHAR(150) NOT NULL,
    pengirim VARCHAR(40) NOT NULL,
    penerima VARCHAR(40) NOT NULL,
    pengguna VARCHAR(40) NOT NULL,
    tanggal_dibuat DATE NOT NULL,
    
    -- Database Related
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP NULL DEFAULT NULL,

    -- Constraints
    PRIMARY KEY(id_surat),
    UNIQUE(no_surat)
);

CREATE TABLE Tipe (
    -- Info
    id_tipe INT NOT NULL AUTO_INCREMENT,
    kode_tipe VARCHAR(5) NOT NULL,
    nama_tipe VARCHAR(10) NOT NULL,

    -- Constraints
    PRIMARY KEY(id_tipe),
    UNIQUE(kode_tipe),
    UNIQUE(nama_tipe)
);

CREATE TABLE IO (
    -- Info
    id_tipe_io INT NOT NULL AUTO_INCREMENT,
    nama_io VARCHAR(10) NOT NULL,

    -- Constraints
    PRIMARY KEY(id_tipe_io),
    UNIQUE(nama_io)
);

-- Tabel berikut digunakan untuk sistem generasi otomatis
-- nilai Surat Keluar. Dapat juga digunakan sebagai alat
-- rekap berapa surat keluar yang pernah diterbitkan.
CREATE TABLE Counter (
    -- Info
    tahun_counter INT NOT NULL,
    counter_value INT NOT NULL,

    -- Constraints
    UNIQUE(tahun_counter),
    PRIMARY KEY(tahun_counter)
);

--
--  Tabel Relasi
--

CREATE TABLE TipeSurat (
    -- Info
    id_tipe INT NOT NULL,
    id_surat INT NOT NULL,

    -- Constraints
    UNIQUE(id_surat),       -- Satu tipe surat boleh dicomot beberapa surat,
                            -- tapi satu surat hanya boleh punya satu tipe.
    -- Keys
    FOREIGN KEY(id_tipe) REFERENCES Tipe(id_tipe),
    FOREIGN KEY(id_surat) REFERENCES Surat(id_surat)
);

CREATE TABLE IOSurat (
    -- Info
    id_tipe_io INT NOT NULL,
    id_surat INT NOT NULL,

    -- Constraints
    UNIQUE(id_surat),       -- Surat keluar jangan jadi surat masuk juga... (?)
                            -- Kalau surat keluar yang ditujukan utk dosen dalam
                            -- ato orang dalam perlu masuk juga ga ya...?

    -- Keys
    FOREIGN KEY(id_tipe_io) REFERENCES IO(id_tipe_io),
    FOREIGN KEY(id_surat) REFERENCES Surat(id_surat)
);

--
-- Views
--

CREATE VIEW DaftarSurat
AS
SELECT
    Surat.no_surat AS no_surat,
    Surat.perihal AS perihal,
    Surat.pengirim AS pengirim,
    Surat.penerima AS penerima,
    Surat.pengguna AS pengguna,
    Surat.tanggal_dibuat AS tanggal_dibuat,
    
    Tipe.kode_tipe AS kode_tipe,
    Tipe.nama_tipe AS nama_tipe,

    IO.nama_io AS nama_io
FROM
    Surat   INNER JOIN TipeSurat
        ON Surat.id_surat = TipeSurat.id_surat
            INNER JOIN Tipe
        ON TipeSurat.id_tipe = Tipe.id_tipe
            INNER JOIN IOSurat
        ON Surat.id_surat = IOSurat.id_surat
            INNER JOIN IO
        ON IO.id_tipe_io = IOSurat.id_tipe_io
;