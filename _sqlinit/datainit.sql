INSERT INTO Tipe
    (kode_tipe, nama_tipe) VALUES
    ('I', 'Internal'),
    ('E', 'Eksternal'),
    ('SK', 'Surat Keterangan'),
    ('ST', 'Surat Tugas'),
    ('SM', 'Surat Mahasiswa'),
    ('SU', 'Surat Undangan');

INSERT INTO IO
    (nama_io) VALUES
    ('Masuk'),
    ('Keluar');