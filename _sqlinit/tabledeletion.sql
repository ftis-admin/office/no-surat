DROP TABLE IF EXISTS
    IOSurat, TipeSurat;

DROP TABLE IF EXISTS
    Surat, Tipe, IO, Counter;

DROP VIEW IF EXISTS
    DaftarSurat;