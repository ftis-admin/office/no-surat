<?php
Namespace Controller;

class App {

    public function get_hello($f3){
        \View\Template::render("welcome.html", "Selamat Datang");
    }

    public function get_cari($f3){
        \View\Template::render("surat-filter.html", "Cari Surat");
    }

    public function get_page($f3){
        if(!file_exists($f3->UI . "page/" . $f3->PARAMS['page'] . ".html"))
            $f3->error(404);
        \View\Template::render("page/" . $f3->PARAMS['page'] . ".html", "");
    }

    public function get_login($f3){
        $f3->page['clean'] = true;
        \View\Template::render("akun-login.html", "Login");
    }

    public function get_logout($f3){
        /* 
            TODO: Add account implementation here
        */
        $f3->reroute("@akun_login");
    }
}