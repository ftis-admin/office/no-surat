<?php
namespace Controller;

class Surat {

    public function get_masuk_new($f3){
        return \View\Template::render("surat-new-masuk.html", "Surat Masuk Baru");
    }

    public function get_keluar_new($f3){
        return \View\Template::render("surat-new-keluar.html", "Surat Keluar Baru");
    }

    public function get_detail($f3){
        /*
            Add some implementation here.
        */
        return \View\Template::render("surat-detil.html", "Detil Surat");
    }

    public function get_cari($f3){
        /*
            Add some implementation here.
        */
        return \View\Template::render("surat-filter-hasil.html", "Surat-surat");
    }

}