<?php
namespace Model;
class Tipe extends \DB\Cortex {
    protected
    $fieldConf = array(
        'kode_tipe'=>[
            'type'=>\DB\SQL\Schema::DT_VARCHAR128
        ],
        'nama_tipe'=>[
            'type'=>\DB\SQL\Schema::DT_VARCHAR128            
        ]
    ),
    $db = 'DB',
    $primary ='id_tipe',
    $table = 'Tipe';
}
?>