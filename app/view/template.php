<?php
namespace View;

class Template {
    public static function render($page_location, $page_title = "Untitled Document", $site_title="No.Surat FTIS") {
        // \F3::instance()->set("page.title", $page_title);
        // \F3::instance()->set("page.loation", $page_location);
        \F3::mset([
            "page.title"   => $page_title . " | " . $site_title,
            "page.location" => $page_location
        ]);
        echo \Template::instance()->render("layout.html");
    }
}